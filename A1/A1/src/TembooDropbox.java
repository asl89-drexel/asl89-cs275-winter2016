import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Utilities.Encoding.Base64Decode;
import com.temboo.Library.Utilities.Encoding.Base64Decode.Base64DecodeInputSet;
import com.temboo.Library.Utilities.Encoding.Base64Decode.Base64DecodeResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TembooDropbox {

    public static void main(String[] args) {
	// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
	// TembooSession session = new TembooSession("andrewlieberman", "myFirstApp", "811456f5270a4dc797e6feae6ec64d4c");
	TembooSession session = null;
	try {
	    session = new TembooSession("andrewlieberman", "myFirstApp", "811456f5270a4dc797e6feae6ec64d4c");
	} catch (TembooException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	}
	initOAuth(session);
	cleanup(session);
    }

    public static void initOAuth(TembooSession session) {
	String tokenSecret, callbackId, initUrl;
	java.awt.Desktop desktop;
	java.net.URI uri;
	InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

	// Get an InputSet object for the choreo
	InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

	// Set credential to use for execution
	initializeOAuthInputs.setCredential("Dropbox1");

	try {
	    // Execute Choreo
	    InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	    initUrl = initializeOAuthResults.get_AuthorizationURL();
	    tokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
	    callbackId = initializeOAuthResults.get_CallbackID();
	    //Let's try opening up the browser automatically
	    if (java.awt.Desktop.isDesktopSupported()) {
		desktop = java.awt.Desktop.getDesktop();
		uri = new java.net.URI(initUrl);
		desktop.browse(uri);
	    } else {
		System.out.println("Copy this link and paste it in your browser:\n\t"
			+ initUrl + "\nBe sure to click \"Allow\"");
	    }
	    //Runs right after authorization in browswer
	    finalOAuth(session, tokenSecret, callbackId);
	    //Need a multicatch for Temboo and java.net.URI functions
	} catch (TembooException | URISyntaxException | IOException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    public static void finalOAuth(TembooSession session, String tokenSecret, String callbackId) {
	FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

	// Get an InputSet object for the choreo
	FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

	// Set credential to use for execution
	finalizeOAuthInputs.setCredential("Dropbox2");

	// Set inputs
	finalizeOAuthInputs.set_OAuthTokenSecret(tokenSecret);
	finalizeOAuthInputs.set_CallbackID(callbackId);

	try {
	    // Execute Choreo
	    FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	    getFile(session);
	} catch (TembooException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    public static void getFile(TembooSession session) {
	String response;
	GetFile getFileChoreo = new GetFile(session);

	// Get an InputSet object for the choreo
	GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

	// Set credential to use for execution
	getFileInputs.setCredential("Dropbox3");

	try {
	    // Execute Choreo
	    GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
	    response = getFileResults.get_Response();
	    decode(session, response);
	} catch (TembooException ex) {
	    System.out.println("__list cannot be found");
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	    System.exit(1);
	}
    }

    public static void decode(TembooSession session, String response) {
	String text;
	Base64Decode base64DecodeChoreo = new Base64Decode(session);

	// Get an InputSet object for the choreo
	Base64DecodeInputSet base64DecodeInputs = base64DecodeChoreo.newInputSet();

	// Set inputs
	base64DecodeInputs.set_Base64EncodedText(response);

	try {
	    // Execute Choreo
	    Base64DecodeResultSet base64DecodeResults = base64DecodeChoreo.execute(base64DecodeInputs);
	    text = base64DecodeResults.get_Text();
	    mapDirs(text);
	} catch (TembooException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    public static void mapDirs(String text) {
	String textLines[], dirAOrigin, dirADest, dirBOrigin, dirBDest;

	textLines = text.split("\t|\n|\r");
	dirAOrigin = textLines[0];
	dirADest = textLines[1];
	//textLines[2] is a blank space because of "\r"'s action
	dirBOrigin = textLines[3];
	dirBDest = textLines[4];
	processFiles(dirAOrigin, dirADest, dirBOrigin, dirBDest);
    }
    
    public static void processFiles(String dirAOrigin, String dirADest, String dirBOrigin, String dirBDest) {	
	Path fileAName, fileBName;
	File fileAOrigin, fileADest, fileBOrigin, fileBDest;
	
	fileAOrigin = new File(dirAOrigin);
	fileAName = fileAOrigin.toPath().getFileName();
	fileADest = new File(dirADest + File.separator + fileAName);
	fileADest.getParentFile().mkdirs();
	fileBOrigin = new File(dirBOrigin);
	fileBName = fileBOrigin.toPath().getFileName();
	fileBDest = new File(dirBDest + File.separator + fileBName);
	fileBDest.getParentFile().mkdirs();
	try {
	    fileADest.createNewFile();
	    Files.copy(fileAOrigin.toPath(), fileADest.toPath(), REPLACE_EXISTING);
	    System.out.println("File " + fileAName + " from path " + fileAOrigin
		    + " successfully copied to " + fileADest);
	    fileBDest.createNewFile();
	    Files.copy(fileBOrigin.toPath(), fileBDest.toPath(), REPLACE_EXISTING);
	    System.out.println("File " + fileBName + " from path " + fileBOrigin
		    + " successfully copied to " + fileBDest);
	} catch (IOException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	    System.exit(1);
	} 
    }
    
    public static void cleanup(TembooSession session) {
	CopyFileOrFolder copyFileOrFolderChoreo = new CopyFileOrFolder(session);

	// Get an InputSet object for the choreo
	CopyFileOrFolderInputSet copyFileOrFolderInputs = copyFileOrFolderChoreo.newInputSet();

	// Set credential to use for execution
	copyFileOrFolderInputs.setCredential("Dropbox4");

	try {
	    // Execute Choreo
	    CopyFileOrFolderResultSet copyFileOrFolderResults = copyFileOrFolderChoreo.execute(copyFileOrFolderInputs);
	    DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

	    // Get an InputSet object for the choreo
	    DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

	    // Set credential to use for execution
	    deleteFileOrFolderInputs.setCredential("Dropbox5");

	    // Execute Choreo
	    DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
	    System.out.println("__list contents deleted\n__list renamed to list");
	} catch (TembooException ex) {
	    Logger.getLogger(TembooDropbox.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
}