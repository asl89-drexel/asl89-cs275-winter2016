import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TwitterSmog {
    private static final String CONSUMER_KEY = "855j1vHl8htt6XWDn1BRweXXz"; 
    private static final String CONSUMER_SECRET = "4j1VO8hhEpRo0OqceVwNHngitOx14ggVKQl3BozeYDImXMXDWg";
    private static final String SCREEN_NAME = "h3h3productions"; //Modifiable
    private static final int NUM_TWEETS = 30; //Modifiable
    private static int NUM_POLYSYLLABIC_WORDS = 0;
    
    public static void main(String[] args) {
        double grade;        
        
	// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
	// TembooSession session = new TembooSession("andrewlieberman", "myFirstApp", "811456f5270a4dc797e6feae6ec64d4c");
	TembooSession session = null;
	try {
	    session = new TembooSession("andrewlieberman", "myFirstApp", "811456f5270a4dc797e6feae6ec64d4c");
	} catch (TembooException ex) {
	    Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
	}
	initOAuth(session);
        grade = getSmog();
        System.out.println("The SMOG grade for user " + SCREEN_NAME + " is " 
                + grade + " with " + NUM_POLYSYLLABIC_WORDS 
                + " polysyllabic words within " + NUM_TWEETS + " tweets.");
    }
    
    public static void initOAuth(TembooSession session) {
	String tokenSecret, callbackId, initUrl;
	Desktop desktop;
	URI uri;
	
	InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
	// Get an InputSet object for the choreo
	InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
	// Set credential to use for execution
	initializeOAuthInputs.setCredential("Twitter1");
	try {
	    // Execute Choreo
	    InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	    initUrl = initializeOAuthResults.get_AuthorizationURL();
	    tokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
	    callbackId = initializeOAuthResults.get_CallbackID();
            //Let's try opening up the browser automatically
	    if(Desktop.isDesktopSupported()) {
		desktop = Desktop.getDesktop();
		uri = new URI(initUrl);
		desktop.browse(uri);
	    } else {
		System.out.println("Copy this link and paste it in your browser:\n\t"
			+ initUrl + "\nBe sure to click \"Allow\"");
	    }
          finalOAuth(session, tokenSecret, callbackId);
	} catch (TembooException | URISyntaxException | IOException ex) {
	    Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    public static void finalOAuth(TembooSession session, String tokenSecret, String callbackId) {
        String accessToken, accessTokenSecret;
        
        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);
        FinalizeOAuthInputSet finalizeOAuthInputs;
        finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
        // Set credential to use for execution
        finalizeOAuthInputs.setCredential("Twitter2");
        // Set inputs
        finalizeOAuthInputs.set_OAuthTokenSecret(tokenSecret);
	finalizeOAuthInputs.set_CallbackID(callbackId);
        try {
            // Execute Choreo
            FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
            accessToken = finalizeOAuthResults.get_AccessToken();
            accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
            getTweets(session, accessToken, accessTokenSecret);
        } catch (TembooException ex) {
            Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void getTweets(TembooSession session, String accessToken, String accessTokenSecret) {
        String response, text;
        JsonParser jPar;
	JsonElement jElm;
        JsonArray jArr;
	JsonObject jObj;
	
        UserTimeline userTimelineChoreo = new UserTimeline(session);
        // Get an InputSet object for the choreo
        UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();
        // Set inputs
	userTimelineInputs.set_ExcludeReplies(true);
        userTimelineInputs.set_Count("200"); //200 is max count of both tweets and replies
        userTimelineInputs.set_AccessToken(accessToken);
        userTimelineInputs.set_AccessTokenSecret(accessTokenSecret);
        userTimelineInputs.set_ConsumerKey(CONSUMER_KEY);
        userTimelineInputs.set_ConsumerSecret(CONSUMER_SECRET);
        userTimelineInputs.set_ScreenName(SCREEN_NAME);
        try {
            // Execute Choreo
            UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
            response = userTimelineResults.get_Response();
	    jPar = new JsonParser();
	    jElm = jPar.parse(response);
	    jArr = jElm.getAsJsonArray();
            //Assume 1 tweet = 1 sentence
	    for(int i = 0; i < NUM_TWEETS; i++) {
		jObj = jArr.get(i).getAsJsonObject();
		text = jObj.get("text").toString();
                polysyllabicWords(text);
            }
	} catch (TembooException ex) {
            Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void polysyllabicWords(String tweet) {
        String[] tweetSplit;
        String tweetRegex;
        int syllables;
        
        tweetSplit = tweet.split(" ");
        for(String s : tweetSplit) {
            tweetRegex = s.replaceAll("[^A-Za-z]+", "");
            if(tweetRegex.equals("") == false) { //Otherwise, the wordnik URL will be an error
                syllables = getSyllables(tweetRegex);
                if(syllables >= 3) {
                    System.out.println(tweetRegex + "\t" + syllables);
                    NUM_POLYSYLLABIC_WORDS++;
                }
            }
        }
    }
        
    public static int getSyllables(String word) {
        URL url;
        URLConnection conn;
        BufferedReader br;
        String line, response = "", seq = "";
        int seqToInt;
        JsonParser jPar;
        JsonElement jElm;
        JsonArray jArr;
	JsonObject jObj;
                
        try {
            url = new URL("http://api.wordnik.com:80/v4/word.json/" + word  
                    + "/hyphenation?useCanonical=false&limit=50&api_key="
                    +"a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5");
            conn = url.openConnection();
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while((line = br.readLine()) != null)
                response += line;
            jPar = new JsonParser();
            jElm = jPar.parse(response);
            jArr = jElm.getAsJsonArray();
            for(int i = 0; i < jArr.size(); i++) {
                jObj = jArr.get(i).getAsJsonObject();
		seq = jObj.get("seq").toString();
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TwitterSmog.class.getName()).log(Level.SEVERE, null, ex);
        }
        try { 
            seqToInt = Integer.parseInt(seq);
        } catch(NumberFormatException e) { 
            seqToInt = 0; //Assume that a null string is 1 syllable
        }
        seqToInt++; //fixes off by one syllable problem
        return seqToInt;
    }
    
    public static double getSmog() {
	double grade;
        
        grade = 1.0430 * Math.sqrt(NUM_POLYSYLLABIC_WORDS * (30 / NUM_TWEETS)) 
                + 3.1291;
        return grade;
    }
}