package cs275.l4;

import android.graphics.Bitmap;

public class RowItem {
    private String date;
    private String cond;
    private String temp;
    private String humi;
    private String icon;
    private Bitmap image;

    public RowItem(String date, String cond, String temp, String humi, String icon, Bitmap image) {
        this.date = date;
        this.cond = cond;
        this.temp = temp;
        this.humi = humi;
        this.icon = icon;
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public String getCond() {
        return cond;
    }

    public String getTemp() {
        return temp;
    }

    public String getHumi() {
        return humi;
    }

    public String getIcon() {
        return icon;
    }

    public Bitmap getImage() {
        return image;
    }
}