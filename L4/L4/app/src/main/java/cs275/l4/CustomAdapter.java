package cs275.l4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<RowItem> {
    Context context;

    public CustomAdapter(Context context, int resource, List<RowItem> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    //Stores each of the component views for immediate access without looking up repeatedly
    private class ViewHolder {
        TextView textDate;
        TextView textCond;
        TextView textTemp;
        TextView textHumi;
        TextView textIcon;
        ImageView imageIcon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        RowItem rowItem;
        LayoutInflater inflater;

        rowItem = getItem(position);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //convertView is the old view to reuse, if possible
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.custom_layout, parent, false);
            holder = new ViewHolder();
            holder.textDate = (TextView)convertView.findViewById(R.id.date);
            holder.textCond = (TextView)convertView.findViewById(R.id.cond);
            holder.textTemp = (TextView)convertView.findViewById(R.id.temp);
            holder.textHumi = (TextView)convertView.findViewById(R.id.humi);
            holder.imageIcon = (ImageView)convertView.findViewById(R.id.iconImage);
        } else
            holder = (ViewHolder)convertView.getTag();

        convertView.setTag(holder);
        holder.textDate.setText(rowItem.getDate());
        holder.textCond.setText(rowItem.getCond());
        holder.textTemp.setText(rowItem.getTemp());
        holder.textHumi.setText(rowItem.getHumi());
        holder.imageIcon.setImageBitmap(rowItem.getImage());
        return convertView;
    }
}