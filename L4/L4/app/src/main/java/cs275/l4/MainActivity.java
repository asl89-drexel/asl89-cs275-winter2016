package cs275.l4;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {
    private GPS gps;
    private double latitude;
    private double longitude;
    private Button button;
    private String errorDesc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.buttonRun);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new GPS(MainActivity.this);

                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    if(latitude != 0.0 || longitude != 0.0) {
                        button.setEnabled(false);
                        button.setVisibility(View.INVISIBLE);
                    }
                    new WeatherTask().execute(Double.toString(latitude),
                            Double.toString(longitude));
                } else
                    gps.showSettingsAlert();
            }
        });
    }

    private class WeatherTask extends AsyncTask<String, String, Void> {
        private List<String> date = new ArrayList<>();
        private List<String> cond = new ArrayList<>();
        private List<String> temp = new ArrayList<>();
        private List<String> humi = new ArrayList<>();
        private List<String> icon = new ArrayList<>();
        private List<Bitmap> image = new ArrayList<>();
        private List<RowItem> rowItems = new ArrayList<>();

        @Override
        protected Void doInBackground(String... params) {
            String latitude, longitude, link, jsonInfo;
            URL url;
            HttpURLConnection httpCon;
            JsonParser jPar;
            JsonElement jElm;
            JsonObject jObj, responseObj;
            JsonArray jArr;
            Boolean containsError = false;
            RowItem item;
            Bitmap bmp;

            latitude = params[0];
            longitude = params[1];
            publishProgress(latitude, longitude);

            try {
                link = "http://api.wunderground.com/api/17032a6bbeffa11e/hourly/q/"
                        + latitude + "," + longitude + ".json";
                url = new URL(link);
                httpCon = (HttpURLConnection)url.openConnection();
                httpCon.connect();
                jPar = new JsonParser();
                jsonInfo = jPar.parse(new InputStreamReader(
                        (InputStream)httpCon.getContent())).toString();
                jElm = new JsonParser().parse(jsonInfo);
                jObj = jElm.getAsJsonObject();

                //Check if an error exists from the response; get its description
                responseObj = jObj.get("response").getAsJsonObject();

                for(Map.Entry<String, JsonElement> e : responseObj.entrySet()) {
                    if(e.getKey().equals("error")) {
                        containsError = true;
                        errorDesc = "ERROR: " + e.getValue().getAsJsonObject().get("description")
                                .toString().replace("\"", "");
                    }
                }

                if(!containsError) {
                    jArr = jObj.getAsJsonArray("hourly_forecast");

                    for (int i = 0; i < jArr.size(); i++) {
                        jObj = jArr.get(i).getAsJsonObject();
                        date.add(jObj.get("FCTTIME").getAsJsonObject().get("pretty").toString()
                                .replace("\"", ""));
                        cond.add("Condition: " + jObj.get("condition").toString().
                                replace("\"", ""));
                        temp.add("Temperature: " + jObj.get("temp").getAsJsonObject().get("english").
                                toString().replace("\"", "") + " °F");
                        humi.add("Humidity: " + jObj.get("humidity").toString()
                                .replace("\"", "") + "%");
                        icon.add(jObj.get("icon_url").toString().replace("\"", ""));
                        url = new URL(icon.get(i));
                        bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        image.add(bmp);
                        item = new RowItem(date.get(i), cond.get(i), temp.get(i), humi.get(i),
                                icon.get(i), image.get(i));
                        rowItems.add(item);
                        publishProgress(date.get(i), cond.get(i), temp.get(i), humi.get(i),
                                image.get(i).toString());
                    }
                } else {
                    publishProgress(errorDesc);
                    this.cancel(true);
                }
            } catch(Exception e) {
                Log.e("ERROR", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            TextView textLocation;
            String message;
            ListAdapter listAdapter;
            ListView listView;
            ColorStateList color;

            textLocation = (TextView)findViewById(R.id.textLocation);
            color = textLocation.getTextColors();

            //Display the user's latitude and longitude
            if(values.length == 2) {
                message = "Checking forecast for\n" + values[0] + ", " + values[1]
                        + "...";
                textLocation.setTextColor(color);
                textLocation.setText(message);
            }

            //Display forecast information when it is available
            for(int i = 2; i < values.length; i++) {
                listView = (ListView)findViewById(R.id.listView);
                listAdapter = new CustomAdapter(MainActivity.this, R.layout.custom_layout, rowItems);
                listView.setAdapter(listAdapter);
            }
        }

        @Override
        protected void onCancelled() {
            TextView textLocation;
            Spanned boldMessage;

            textLocation = (TextView)findViewById(R.id.textLocation);
            boldMessage = Html.fromHtml("<h2>" + errorDesc + "</h2>");
            textLocation.setTextColor(Color.rgb(255, 0, 0));
            textLocation.setText(boldMessage);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            TextView textLocation;
            Spanned boldMessage;

            textLocation = (TextView)findViewById(R.id.textLocation);
            boldMessage = Html.fromHtml("<h2>Finished</h2>");
            textLocation.setTextColor(Color.rgb(69, 231, 138));
            textLocation.setText(boldMessage);
        }
    }
}