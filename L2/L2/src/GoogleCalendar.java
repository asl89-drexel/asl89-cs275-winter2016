import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GoogleCalendar {
    public String getClientId() {
        JsonParser jp;
        JsonElement root;
        JsonObject rootObj;
        String clientId = "";
        try {
            jp = new JsonParser();
            root = jp.parse(new FileReader("client_secret.json"));
            rootObj = root.getAsJsonObject();
            clientId = rootObj.get("installed").getAsJsonObject().get("client_id").getAsString();
            return clientId;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientId;
    }
    
    public String getClientSecret() {
        JsonParser jp;
        JsonElement root;
        JsonObject rootObj;
        String clientSecret = "";
        try {
            jp = new JsonParser();
            root = jp.parse(new FileReader("client_secret.json"));
            rootObj = root.getAsJsonObject();
            clientSecret = rootObj.get("installed").getAsJsonObject().get("client_secret").getAsString();
            return clientSecret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientSecret;
    }
    
    public String getRedirectUri() {
        JsonParser jp;
        JsonElement root;
        JsonObject rootObj;
        String redirectUri = "";
        try {
            jp = new JsonParser();
            root = jp.parse(new FileReader("client_secret.json"));
            rootObj = root.getAsJsonObject();
            redirectUri = rootObj.get("installed").getAsJsonObject()
		    .get("redirect_uris").getAsJsonArray().get(0).getAsString();
            return redirectUri;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoogleCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return redirectUri;
    }
}