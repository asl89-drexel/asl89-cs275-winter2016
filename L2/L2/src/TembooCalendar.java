import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TembooCalendar {
    public String authUrl, callbackId;
    TembooSession session;
    

    public TembooCalendar() {
        try {
            this.session = new TembooSession("andrewlieberman", "myFirstApp", "811456f5270a4dc797e6feae6ec64d4c");
        } catch (TembooException ex) {
            Logger.getLogger(TembooCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getAuthUrl() {
	return authUrl;
    }
    
    public String getCallbackId() {
	return callbackId;
    }
    
    public void initOAuth() {
        InitializeOAuthResultSet initializeOAuthResults;        
        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        // Get an InputSet object for the choreo
        InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set credential to use for execution
        initializeOAuthInputs.setCredential("Profile2");

        try {
            // Execute Choreo
            initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
            authUrl = initializeOAuthResults.get_AuthorizationURL();
	    callbackId = initializeOAuthResults.get_CallbackID();
        } catch (TembooException ex) {
            Logger.getLogger(TembooCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void finalizeOAuth(String callbackId, int timeout) {
        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_ClientSecret("HOmGAG9V6y0ozBe2KYHi-ceR");
        finalizeOAuthInputs.set_CallbackID(callbackId);
        finalizeOAuthInputs.set_ClientID("424325710128-5n25ho2dvsfsa32rmvptesbfgdleh9tm.apps.googleusercontent.com");
        finalizeOAuthInputs.set_Timeout(timeout);
        
        try {
            // Execute Choreo
            FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
        } catch (TembooException ex) {
            Logger.getLogger(TembooCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getCalendar() {
	String id, summary, created;
	JsonParser jp;
        JsonElement root;
        JsonObject rootObj, item;
	JsonArray items;
	GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

	// Get an InputSet object for the choreo
	GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

	// Set credential to use for execution
	getAllEventsInputs.setCredential("Profile4");

	try {
	    // Execute Choreo
	    GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
	    jp = new JsonParser();
	    root = jp.parse(getAllEventsResults.get_Response());
	    rootObj = root.getAsJsonObject();
	    items = rootObj.get("items").getAsJsonArray();
	    for(int i = 0; i < items.size(); i++) {
		item = items.get(i).getAsJsonObject();
		id = item.get("id").getAsString();
		System.out.println("#" + i + "\tid:\t\t" + id);
		if(item.has("summary")) {
		    summary = item.get("summary").getAsString();
		    System.out.println("\tsummary:\t" + summary);
		}
		if(item.has("created")) {
		    created = item.get("created").getAsString();
		    System.out.println("\tcreated:\t" + created);
		}
		System.out.println("");
	    }
	} catch (TembooException ex) {
	    Logger.getLogger(TembooCalendar.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
}