import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import java.io.IOException;

public class RunCalendars {
    public static void main(String[] args) { 
	//Comment out the API function you don't want to use
        
	//Paste the generated code as the arg (from the web browser)
        //The code will change every time you run the program
        //googleCal("4/WPgaXjI0b7EuZnoCeJLGlFDKrHOz81aK9A5JSJYFCO0");
        
        tembooCal(120);
    }
    
    public static void googleCal(String code) {
        GoogleCalendar goog;
        TokenResponse response;
        String clientId, clientSecret, redirectUri, redirectSite;

        goog = new GoogleCalendar();
        clientId = goog.getClientId();
        clientSecret = goog.getClientSecret();
        redirectUri = goog.getRedirectUri();
        redirectSite = "https://accounts.google.com/o/oauth2/auth?access_type="
                + "offline&client_id=" + clientId + "&scope=https://www.google"
                + "apis.com/auth/calendar&response_type=code&redirect_uri=" + 
                redirectUri + "&state=/profile&approval_prompt=force";
       
        try {
            response = new AuthorizationCodeTokenRequest(
                    new NetHttpTransport(),
                    new JacksonFactory(),
                    new GenericUrl("https://accounts.google.com/o/oauth2/token"), 
                    code) 
                    .setRedirectUri(redirectUri)
                    .set("client_id", clientId)
                    .set("client_secret", clientSecret)
                    .execute();
            System.out.println("Access token:\n\t" + response.getAccessToken());
        } catch (IOException ex) {
            System.out.println("Your access token has expired\n" + 
                    "Copy this link and paste it in your browser:\n\t" 
                    + redirectSite +
                    "\nBe sure to click \"Allow\"");
        } 
    }
    
    public static void tembooCal(int timeout) {
        TembooCalendar temb;
        String authUrl, callbackId;
        
        temb = new TembooCalendar();
        temb.initOAuth();
        authUrl = temb.getAuthUrl();
	callbackId = temb.getCallbackId();
        
        System.out.println("Copy this link and paste it in your browser:\n\t"
                + authUrl + 
                "\nBe sure to click \"Allow\"");
        
        temb.finalizeOAuth(callbackId, timeout);
	temb.getCalendar();
    }
}