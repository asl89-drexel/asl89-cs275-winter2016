package cs275.l3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

//Implementing OnClickListener in order to handle onClick() for multiple buttons
public class MainActivity extends AppCompatActivity implements OnClickListener {
    private Button button1, button2, button3, button4, button5, button6, button7, button8, button9;
    private List<Integer> gameGrid;
    private List<Integer> player1;
    private List<Integer> player2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String message;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message = "Player 1 is \"X\"\nPlayer 2 is \"O\"";
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

        //Indices changed to 1 represent an already clicked button
        gameGrid = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);
        player1  = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);
        player2  = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);

        button1 = (Button)findViewById(R.id.button1);
        button2 = (Button)findViewById(R.id.button2);
        button3 = (Button)findViewById(R.id.button3);
        button4 = (Button)findViewById(R.id.button4);
        button5 = (Button)findViewById(R.id.button5);
        button6 = (Button)findViewById(R.id.button6);
        button7 = (Button)findViewById(R.id.button7);
        button8 = (Button)findViewById(R.id.button8);
        button9 = (Button)findViewById(R.id.button9);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
    }

    //Player 1 will be "X", Player 2 will be "O"
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button1:
                if(isP1Turn() == true) {
                    button1.setText("X");
                    player1.set(0, 1);
                } else {
                    button1.setText("O");
                    player2.set(0, 1);
                }
                button1.setClickable(false);
                gameGrid.set(0, 1);
                break;
            case R.id.button2:
                if(isP1Turn() == true) {
                    button2.setText("X");
                    player1.set(1, 1);
                } else {
                    button2.setText("O");
                    player2.set(1, 1);
                }
                button2.setClickable(false);
                gameGrid.set(1, 1);
                break;
            case R.id.button3:
                if(isP1Turn()) {
                    button3.setText("X");
                    player1.set(2, 1);
                } else {
                    button3.setText("O");
                    player2.set(2, 1);
                }
                button3.setClickable(false);
                gameGrid.set(2, 1);
                break;
            case R.id.button4:
                if(isP1Turn()) {
                    button4.setText("X");
                    player1.set(3, 1);
                } else {
                    button4.setText("O");
                    player2.set(3, 1);
                }
                button4.setClickable(false);
                gameGrid.set(3, 1);
                break;
            case R.id.button5:
                if(isP1Turn()) {
                    button5.setText("X");
                    player1.set(4, 1);
                } else {
                    button5.setText("O");
                    player2.set(4, 1);
                }
                button5.setClickable(false);
                gameGrid.set(4, 1);
                break;
            case R.id.button6:
                if(isP1Turn()) {
                    button6.setText("X");
                    player1.set(5, 1);
                } else {
                    button6.setText("O");
                    player2.set(5, 1);
                }
                button6.setClickable(false);
                gameGrid.set(5, 1);
                break;
            case R.id.button7:
                if(isP1Turn()) {
                    button7.setText("X");
                    player1.set(6, 1);
                } else {
                    button7.setText("O");
                    player2.set(6, 1);
                }
                button7.setClickable(false);
                gameGrid.set(6, 1);
                break;
            case R.id.button8:
                if(isP1Turn()) {
                    button8.setText("X");
                    player1.set(7, 1);
                } else {
                    button8.setText("O");
                    player2.set(7, 1);
                }
                button8.setClickable(false);
                gameGrid.set(7, 1);
                break;
            case R.id.button9:
                if(isP1Turn()) {
                    button9.setText("X");
                    player1.set(8, 1);
                } else {
                    button9.setText("O");
                    player2.set(8, 1);
                }
                button9.setClickable(false);
                gameGrid.set(8, 1);
                break;
            default:
                break;
        }
        checkWinner();
    }

    //Player 1's turn will be an even-numbered turn
    public boolean isP1Turn() {
        int numOnes;
        numOnes = 0;
        for(int i : gameGrid)
            if(i == 1)
                numOnes++;
        return numOnes % 2 == 0;
    }

    public void checkWinner() {
        TextView text;
        int[] p1, p2, grid, tied;

        p1   = new int[player1.size()];
        p2   = new int[player2.size()];
        grid = new int[gameGrid.size()];
        tied = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1};
        text = (TextView)findViewById(R.id.gameStatText);

        for(int i = 0; i < p1.length; i++)
            p1[i] = player1.get(i);
        for(int i = 0; i < p2.length; i++)
            p2[i] = player2.get(i);
        for(int i = 0; i < grid.length; i++)
            grid[i] = gameGrid.get(i);

        if((p1[0] == 1 && p1[1] == 1 && p1[2] == 1) ||      //Horizontal win
                (p1[3] == 1 && p1[4] == 1 && p1[5] == 1) || //Horizontal win
                (p1[6] == 1 && p1[7] == 1 && p1[8] == 1) || //Horizontal win
                (p1[0] == 1 && p1[3] == 1 && p1[6] == 1) || //Vertical win
                (p1[1] == 1 && p1[4] == 1 && p1[7] == 1) || //Vertical win
                (p1[2] == 1 && p1[5] == 1 && p1[8] == 1) || //Vertical win
                (p1[0] == 1 && p1[4] == 1 && p1[8] == 1) || //Diagonal win
                (p1[2] == 1 && p1[4] == 1 && p1[6] == 1)) { //Diagonal win
            text.setText("Player 1 wins!");
            enableButtons(false);
        } else if((p2[0] == 1 && p2[1] == 1 && p2[2] == 1) || //Horizontal win
                (p2[3] == 1 && p2[4] == 1 && p2[5] == 1) ||   //Horizontal win
                (p2[6] == 1 && p2[7] == 1 && p2[8] == 1) ||   //Horizontal win
                (p2[0] == 1 && p2[3] == 1 && p2[6] == 1) ||   //Vertical win
                (p2[1] == 1 && p2[4] == 1 && p2[7] == 1) ||   //Vertical win
                (p2[2] == 1 && p2[5] == 1 && p2[8] == 1) ||   //Vertical win
                (p2[0] == 1 && p2[4] == 1 && p2[8] == 1) ||   //Diagonal win
                (p2[2] == 1 && p2[4] == 1 && p2[6] == 1)) {   //Diagonal win
            text.setText("Player 2 wins!");
            enableButtons(false);
        } else if(Arrays.equals(grid, tied)) {
            text.setText("It's a tie!");
            enableButtons(false);
        }
    }

    public void enableButtons(boolean b) {
        button1.setClickable(b);
        button2.setClickable(b);
        button3.setClickable(b);
        button4.setClickable(b);
        button5.setClickable(b);
        button6.setClickable(b);
        button7.setClickable(b);
        button8.setClickable(b);
        button9.setClickable(b);
    }

    public void resetGame(View v) {
        String message;
        TextView text;

        message = "Player 1 is \"X\"\nPlayer 2 is \"O\"";
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        text = (TextView)findViewById(R.id.gameStatText);
        text.setText("");
        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        button5.setText("");
        button6.setText("");
        button7.setText("");
        button8.setText("");
        button9.setText("");
        gameGrid = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);
        player1  = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);
        player2  = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0);
        enableButtons(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}