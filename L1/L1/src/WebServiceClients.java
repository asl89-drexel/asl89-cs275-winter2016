import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
        
public class WebServiceClients {
    public static void main(String[] args) {
        String city, state;
        
        //webpage();
        city = city();
        state = state();
        forecast(city, state);
    }
    
    public static void webpage() {
        try {
            URL url;
            URLConnection connect;
            BufferedReader br;
            String line;
            
            //Exceptions needed for URL object and openConnection()
            url = new URL("https://www.cs.drexel.edu/~augenbdh/cs275_wi16/labs/wxunderground.html");
            connect = url.openConnection();
            br = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            
            while((line = br.readLine()) != null) 
                System.out.println(line);
            
            br.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String city() {
        URL url;               
        InputStream is;
        JsonParser parser;
        Event e;
        String city = "";
        
        try {
            //Exceptions needed for URL object and openStream()
            url = new URL("http://api.wunderground.com/api/17032a6bbeffa11e/geolookup/q/autoip.json");
            is = url.openStream();
            parser = Json.createParser(is);
            
            while(parser.hasNext()) {
                e = parser.next();
                
                if(e == Event.KEY_NAME) 
                    if(parser.getString().equals("city")) {
                        parser.next();
                        city = parser.getString();
                        break;
                    }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return city;
    }
    
    public static String state() {
        URL url;
        InputStream is;
        JsonParser parser;
        Event e;
        String state = "";
        
        //Exceptions needed for URL object and openStream()
        try {
            url = new URL("http://api.wunderground.com/api/17032a6bbeffa11e/geolookup/q/autoip.json");
            is = url.openStream();
            parser = Json.createParser(is);
            
            while(parser.hasNext()) {
                e = parser.next();
                
                if(e == Event.KEY_NAME) 
                    if(parser.getString().equals("state")) {
                        parser.next();
                        state = parser.getString();
                        break;
                    }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return state;
    }
    
    public static void forecast(String city, String state) {
        URL url;
        InputStream is;
        JsonParser parser;
        Event e;
        
        //Exceptions needed for URL object and openStream()
        try {
            url = new URL("http://api.wunderground.com/api/17032a6bbeffa11e/forecast/q/" + state + "/" + city + ".json");
            is = url.openStream();
            parser = Json.createParser(is);
            
            while(parser.hasNext()) {
                e = parser.next();
                
                if(e == Event.KEY_NAME) {
                    switch(parser.getString()) {
                        case "date":
                            parser.next();
                            System.out.println("Date: " + parser.getString());
                            break;
                        case "period":
                            parser.next();
                            System.out.println();
                            System.out.println("Period: " + parser.getString());
                            break;
                        case "title":
                            parser.next();
                            System.out.println("Time: " + parser.getString());
                            break;                            
                        case "fcttext":
                            parser.next();
                            System.out.println("Description: " + parser.getString());
                            break;
                        case "pop":
                            parser.next();
                            System.out.println("Probability of Precipitation: " + parser.getString() + "%");
                    }
                    
                    //IllegalStateException if this isn't here
                    if(parser.getString().equals("simpleforecast"))
                        break;
                }
            }
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebServiceClients.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}